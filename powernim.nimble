# Package

version       = "0.0.1"
author        = "Will Reed"
description   = "Basic power menu for Linux (with systemd)"
license       = "BSD-2-Clause"
srcDir        = "src"
bin           = @["powernim"]
backend       = "c"

# Dependencies

requires "nim >= 1.6.6"
requires "nigui"
