import std/[os,osproc,strformat];
import nigui;

when isMainModule:

  let user = getEnv("USER");

  proc logout(name:string) =
    discard execCmdEx("sync");
    discard execCmdEx(&"loginctl kill-user {name}");
    quit(0);

  proc reboot() =
    discard execCmdEx("sync");
    discard execCmdEx("reboot");
    quit(0);

  proc shutdown() =
    discard execCmdEx("sync");
    discard execCmdEx("shutdown now");
    quit(0);

  app.init();

  var frame = newWindow("Powermenu");

  frame.width  = 205;
  frame.height = 280;
  frame.alwaysOnTop = true;
  frame.centerOnScreen();

  var mainContainer = newLayoutContainer(Layout_Vertical);
  mainContainer.padding = 10;
  mainContainer.widthMode = WidthMode_Auto;
  frame.add(mainContainer);


  var inputContainer = newLayoutContainer(Layout_Vertical);
  mainContainer.add(inputContainer);

  var logoutbutton   = newButton("Logout");
  var rebootbutton   = newButton("Reboot");
  var shutdownbutton = newButton("Shutdown");
  var exitbutton     = newButton("Exit");

  logoutbutton.height     = 60;
  logoutbutton.width      = 180;
  logoutbutton.fontFamily = "Play";
  logoutbutton.fontSize   = 24;

  rebootbutton.height     = 60;
  rebootbutton.width      = 180;
  rebootbutton.fontFamily = "Play";
  rebootbutton.fontSize   = 24;

  shutdownbutton.height     = 60;
  shutdownbutton.width      = 180;
  shutdownbutton.fontFamily = "Play";
  shutdownbutton.fontSize   = 24;

  exitbutton.height     = 60;
  exitbutton.width      = 180;
  exitbutton.fontFamily = "Play";
  exitbutton.fontSize   = 24;

  inputContainer.add(logoutbutton);
  inputContainer.add(rebootbutton);
  inputContainer.add(shutdownbutton);
  inputContainer.add(exitbutton);

  logoutbutton.onClick = proc(event: ClickEvent) =
    logout(user);

  rebootbutton.onClick = proc(event: ClickEvent) =
    reboot();

  shutdownbutton.onClick = proc(event: ClickEvent) =
    shutdown();

  exitbutton.onClick = proc(event: ClickEvent) =
    quit(0);

  frame.show();
  exitbutton.focus();

  app.run();
